# Let's Go
This is an implementation of the Snippetbox project described in the book [Let's Go](https://lets-go.alexedwards.net/) by Alex Edwards. The application is a snippet manager - it allows users to sign in and upload new snippets, which can be viewed by anyone. Snippets can be configured to expire on a certain date. The application has unit and integration tests.

## Notes
- Some components are organized slightly differently from the book, but the features are the same
- All static assets are embedded into the binary, making it dependency-free
