package models

import (
	"errors"
	"time"
)

var (
	ErrNoRecord           = errors.New("models: no matching record found")
	ErrInvalidCredentials = errors.New("models: invalid credentials")
	ErrDuplicateEmail     = errors.New("models: duplicate email")
)

type Snippet struct {
	ID      int
	Title   string
	Content string
	Created time.Time
	Expires time.Time
}

type User struct {
	ID             int
	Name           string
	Email          string
	HashedPassword []byte
	Created        time.Time
	Active         bool
}

type SnippetModel interface {
	Insert(string, string, string) (int, error)
	Get(int) (*Snippet, error)
	Latest() ([]*Snippet, error)
}

type UserModel interface {
	Insert(string, string, string) error
	Authenticate(string, string) (int, error)
	Get(int) (*User, error)
}
