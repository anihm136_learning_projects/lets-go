package main

import (
	"net/http"

	"gitlab.com/anihm136_learning_projects/lets-go/ui"
	"github.com/bmizerany/pat"
	"github.com/justinas/alice"
)

func (app *application) routes(config *Config) http.Handler {
	commonMiddleware := alice.New(app.recoverFromPanics, app.logRequest, addSecurityHeaders)
	dynamicMiddleware := alice.New(app.session.Enable, blockCSRF, app.authenticate)

	mux := pat.New()
	mux.Get("/", dynamicMiddleware.ThenFunc(http.HandlerFunc(app.home)))
	mux.Get("/snippet/create", dynamicMiddleware.Append(app.requireAuthentication).ThenFunc(app.createSnippetForm))
	mux.Post("/snippet/create", dynamicMiddleware.Append(app.requireAuthentication).ThenFunc(app.createSnippet))
	mux.Get("/snippet/:id", dynamicMiddleware.ThenFunc(app.showSnippet))
	mux.Get("/user/signup", dynamicMiddleware.ThenFunc(app.signupUserForm))
	mux.Get("/user/signup", dynamicMiddleware.ThenFunc(app.signupUserForm))
	mux.Post("/user/signup", dynamicMiddleware.ThenFunc(app.signupUser))
	mux.Get("/user/login", dynamicMiddleware.ThenFunc(app.loginUserForm))
	mux.Post("/user/login", dynamicMiddleware.ThenFunc(app.loginUser))
	mux.Post("/user/logout", dynamicMiddleware.ThenFunc(app.logoutUser))
	mux.Get("/ping", http.HandlerFunc(ping))

	// fileServer := http.FileServer(http.Dir(config.staticDir))
	fileServer := http.FileServer(http.FS(ui.Files))
	// mux.Get("/static/", http.StripPrefix("/static", fileServer))
	mux.Get("/static/", fileServer)

	return commonMiddleware.Then(mux)
}
