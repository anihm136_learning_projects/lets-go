package main

import (
	"crypto/tls"
	"database/sql"
	"flag"
	"html/template"
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/anihm136_learning_projects/lets-go/pkg/models"
	"gitlab.com/anihm136_learning_projects/lets-go/pkg/models/mysql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/golangcollege/sessions"
)

type Config struct {
	address string
	// staticDir     string
	dsn           string
	sessionSecret string
	useTLS        bool
	tlsKey        string
	tlsCert       string
}

type application struct {
	errLogger     *log.Logger
	infoLogger    *log.Logger
	session       *sessions.Session
	snippets      models.SnippetModel
	templateCache map[string]*template.Template
	users         models.UserModel
}

func main() {
	var cfg Config
	flag.StringVar(&cfg.address, "address", "localhost:4000", "Network address to listen on")
	// flag.StringVar(&cfg.staticDir, "static_dir", "./ui/static/", "Path to directory to serve static assets from")
	flag.StringVar(&cfg.dsn, "dsn", "snippetbox:snippetani123@/snippetbox?parseTime=true", "MySQL data source name")
	flag.StringVar(&cfg.sessionSecret, "secret", "s6Ndh+pPbnzHbS*+9Pk8qGWhTzbpa@ge", "Session secret key")
	flag.BoolVar(&cfg.useTLS, "use_tls", true, "Use TLS (to enable HTTPS)")
	flag.StringVar(&cfg.tlsKey, "tls_key", "./tls/server.key", "Path to TLS key")
	flag.StringVar(&cfg.tlsCert, "tls_cert", "./tls/server.crt", "Path to TLS cert")
	flag.Parse()

	infoLogger := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errLogger := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	db, err := openDb(cfg.dsn)
	if err != nil {
		errLogger.Fatal(err)
	}
	defer db.Close()

	// templateCache, err := newTemplateCache("./ui/html")
	templateCache, err := newTemplateCache()
	if err != nil {
		errLogger.Fatal(err)
	}

	session := sessions.New([]byte(cfg.sessionSecret))
	session.Lifetime = 12 * time.Hour

	app := &application{
		errLogger:     errLogger,
		infoLogger:    infoLogger,
		session:       session,
		snippets:      &mysql.SnippetModel{DB: db},
		templateCache: templateCache,
		users:         &mysql.UserModel{DB: db},
	}

	tlsConfig := &tls.Config{
		CurvePreferences: []tls.CurveID{tls.X25519, tls.CurveP256},
	}

	srv := &http.Server{
		Addr:         cfg.address,
		ErrorLog:     errLogger,
		Handler:      app.routes(&cfg),
		TLSConfig:    tlsConfig,
		IdleTimeout:  time.Minute,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	infoLogger.Printf("Starting server on %s\n", cfg.address)
	if cfg.useTLS {
		err = srv.ListenAndServeTLS(cfg.tlsCert, cfg.tlsKey)
	} else {
		err = srv.ListenAndServe()
	}
	errLogger.Fatal(err)
}

func openDb(dsn string) (*sql.DB, error) {
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	return db, nil
}
