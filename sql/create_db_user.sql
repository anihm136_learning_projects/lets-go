BEGIN WORK;
CREATE USER 'snippetbox'@'localhost';
GRANT SELECT, INSERT, UPDATE ON snippetbox.* TO 'snippetbox'@'localhost';
ALTER USER 'snippetbox'@'localhost' IDENTIFIED BY 'snippetani123';
COMMIT;
