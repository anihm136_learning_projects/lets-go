BEGIN WORK;
CREATE USER 'test_snippetbox'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE, ALTER, DROP, CREATE ON test_snippetbox.* TO 'test_snippetbox'@'localhost';
ALTER USER 'test_snippetbox'@'localhost' IDENTIFIED BY 'snippetani123';
COMMIT;
